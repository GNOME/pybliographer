# pybliographer
# Copyright (C) 2000,2010 Free Software Foundation, Inc.
# Yukihiro Nakai <nakai@gnome.gr.jp>, 2000.
# Takayuki KUSANO <AE5T-KSN@asahi-net.or.jp>, 2010.
#
msgid ""
msgstr ""
"Project-Id-Version: pybliographer master\n"
"Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?"
"product=pybliographer&keywords=I18N+L10N&component=general\n"
"POT-Creation-Date: 2012-01-08 04:35+0000\n"
"PO-Revision-Date: 2012-01-08 14:38+0900\n"
"Last-Translator: OKANO Takayoshi <kano@na.rim.or.jp>\n"
"Language-Team: Japanese <gnome-translation@gnome.gr.jp>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../Pyblio/Autoload.py:78
#, python-format
msgid "warning: can't import %s: %s"
msgstr "注意: %sをインポートできません: %s"

#: ../Pyblio/Config.py:52
#, python-format
msgid "value of `%s' should be of type %s"
msgstr "`%s'の値はタイプ%sでなければなりません"

#: ../Pyblio/Config.py:212
msgid "String"
msgstr "文字"

#: ../Pyblio/Config.py:221
msgid "Boolean"
msgstr "真偽値"

#: ../Pyblio/Config.py:240
msgid "Integer"
msgstr "整数"

#: ../Pyblio/Config.py:242
#, python-format
msgid "Integer under %d"
msgstr "%d未満の整数"

#: ../Pyblio/Config.py:244
#, python-format
msgid "Integer over %d"
msgstr "%dを越える整数"

#: ../Pyblio/Config.py:246
#, python-format
msgid "Integer between %d and %d"
msgstr "%dと%dの間の整数"

#: ../Pyblio/Config.py:258
#, python-format
msgid "Element in `%s'"
msgstr "`%s'の要素"

#: ../Pyblio/Config.py:278
#, python-format
msgid "Tuple (%s)"
msgstr "タプル (%s)"

#: ../Pyblio/Config.py:299
#, python-format
msgid "List (%s)"
msgstr "リスト (%s)"

#: ../Pyblio/Config.py:324
#, python-format
msgid "Dictionary (%s, %s)"
msgstr "辞書 (%s, %s)"

#: ../Pyblio/Config.py:344
#, python-format
msgid "warning: could not restore setting %r to %r: %s"
msgstr ""

#: ../Pyblio/Fields.py:261
msgid "Illegal year value"
msgstr "不正な年号の値"

#: ../Pyblio/Fields.py:265
msgid "Illegal month value"
msgstr "不正な月の値"

#: ../Pyblio/Fields.py:269
msgid "Illegal day value"
msgstr "不正な日付の値"

#: ../Pyblio/Fields.py:510
#, python-format
msgid "in %s"
msgstr "%sの中で"

#: ../Pyblio/Format/BibTeX.py:323 ../Pyblio/Format/BibTeX.py:371
#, python-format
msgid "key `%s' is malformed"
msgstr "キー `%s'は不正です"

#: ../Pyblio/Format/BibTeX.py:340 ../Pyblio/Format/BibTeX.py:359
#, python-format
msgid "%s:%d: key `%s' already defined"
msgstr "%s:%d: キー`%s'はすでに定義されています"

#: ../Pyblio/GnomeUI/Config.py:147 ../Pyblio/GnomeUI/Fields.py:417
msgid ""
"Some changes require to restart Pybliographic\n"
"to be correctly taken into account"
msgstr ""
"いくつかの変更は、正しく反映するためには\n"
"Pybliographicを再起動する必要があります"

#: ../Pyblio/GnomeUI/Config.py:429
msgid "Add"
msgstr "追加"

#: ../Pyblio/GnomeUI/Config.py:432
msgid "Update"
msgstr "更新"

#: ../Pyblio/GnomeUI/Config.py:435 ../Pyblio/GnomeUI/Config.py:549
msgid "Remove"
msgstr "削除"

#: ../Pyblio/GnomeUI/Config.py:546
msgid "Set"
msgstr "設定"

#: ../Pyblio/GnomeUI/Config.py:561
msgid "Key:"
msgstr "キー:"

#: ../Pyblio/GnomeUI/Config.py:563
msgid "Value:"
msgstr "値:"

#. id     stock            label         accel   tooltip   callback
#: ../Pyblio/GnomeUI/Document.py:145
msgid "_File"
msgstr "ファイル(_F)"

#: ../Pyblio/GnomeUI/Document.py:146 ../Pyblio/GnomeUI/Document.py:195
msgid "_Edit"
msgstr "編集(_E)"

#: ../Pyblio/GnomeUI/Document.py:147
msgid "_View"
msgstr "表示(_V)"

#: ../Pyblio/GnomeUI/Document.py:148
msgid "_Cite"
msgstr "引用(_C)"

#: ../Pyblio/GnomeUI/Document.py:149
msgid "_Settings"
msgstr "設定(_S)"

#: ../Pyblio/GnomeUI/Document.py:150
msgid "_Help"
msgstr "ヘルプ(_H)"

#: ../Pyblio/GnomeUI/Document.py:151
msgid "Recent documents"
msgstr "最近開いたドキュメント"

#: ../Pyblio/GnomeUI/Document.py:154
msgid "Open a file"
msgstr "ファイルを開く"

#: ../Pyblio/GnomeUI/Document.py:155
msgid "Open _Location"
msgstr "場所を開く(_L)"

#: ../Pyblio/GnomeUI/Document.py:156
msgid "Save the current file"
msgstr "現在のファイルを保存"

#: ../Pyblio/GnomeUI/Document.py:161
#: ../Pyblio/GnomeUI/glade/pyblio.glade.in.h:8
msgid "Merge With..."
msgstr "結合..."

#: ../Pyblio/GnomeUI/Document.py:162
#: ../Pyblio/GnomeUI/glade/pyblio.glade.in.h:7
msgid "Medline Query..."
msgstr ""

#: ../Pyblio/GnomeUI/Document.py:170
msgid "Add a new entry"
msgstr "新規エントリの作成"

#: ../Pyblio/GnomeUI/Document.py:174
#: ../Pyblio/GnomeUI/glade/pyblio.glade.in.h:12
msgid "S_ort..."
msgstr "ソート(_O)..."

#: ../Pyblio/GnomeUI/Document.py:175
#: ../Pyblio/GnomeUI/glade/pyblio.glade.in.h:2
msgid "Cite..."
msgstr "引用..."

#: ../Pyblio/GnomeUI/Document.py:175
msgid "Cite key(s)"
msgstr ""

#: ../Pyblio/GnomeUI/Document.py:176
#: ../Pyblio/GnomeUI/glade/pyblio.glade.in.h:6
msgid "Format..."
msgstr ""

#: ../Pyblio/GnomeUI/Document.py:178
#: ../Pyblio/GnomeUI/glade/pyblio.glade.in.h:4
msgid "Fields..."
msgstr "フィールド..."

#: ../Pyblio/GnomeUI/Document.py:180
#: ../Pyblio/GnomeUI/glade/pyblio.glade.in.h:5
msgid "Forget all changes"
msgstr ""

#: ../Pyblio/GnomeUI/Document.py:183
msgid "_Resource"
msgstr ""

#: ../Pyblio/GnomeUI/Document.py:194
msgid "_About"
msgstr "情報(_A)"

#: ../Pyblio/GnomeUI/Document.py:245
msgid "Quick search"
msgstr ""

#: ../Pyblio/GnomeUI/Document.py:319
msgid "Unnamed bibliographic database"
msgstr ""

#: ../Pyblio/GnomeUI/Document.py:402
#, python-format
msgid ""
"can't open file `%s' for writing:\n"
"%s"
msgstr ""
"ファイル`%s'を書き込んでいます:\n"
"%s"

#: ../Pyblio/GnomeUI/Document.py:423
#, python-format
msgid ""
"Error while parsing `%s':\n"
"%s"
msgstr ""
"`%s'のパースでエラーが発生しました:\n"
"%s"

#: ../Pyblio/GnomeUI/Document.py:439
msgid "New database"
msgstr "新規データベース"

#: ../Pyblio/GnomeUI/Document.py:447 ../Pyblio/GnomeUI/Document.py:451
msgid "[no entry]"
msgstr "[エントリなし]"

#: ../Pyblio/GnomeUI/Document.py:448
msgid "[1 entry]"
msgstr "[1エントリ]"

#: ../Pyblio/GnomeUI/Document.py:449
#, python-format
msgid "[%d entries]"
msgstr "[%dエントリ]"

#: ../Pyblio/GnomeUI/Document.py:452
#, python-format
msgid "[%d/1 entry]"
msgstr "[%d/1エントリ]"

#: ../Pyblio/GnomeUI/Document.py:453
#, python-format
msgid "[%d/%d entries]"
msgstr "[%d/%dエントリ]"

#: ../Pyblio/GnomeUI/Document.py:458
msgid "[modified]"
msgstr "[変更されました]"

#: ../Pyblio/GnomeUI/Document.py:461
#, python-format
msgid "view limited to: %s"
msgstr ""

#: ../Pyblio/GnomeUI/Document.py:474
msgid ""
"The database has been modified.\n"
"Discard changes?"
msgstr ""
"データベースは変更されました。\n"
"変更を破棄しますか?"

#: ../Pyblio/GnomeUI/Document.py:478
msgid ""
"The database has been modified.\n"
"Save changes?"
msgstr ""
"データベースは変更されました。\n"
"変更を保存しますか?"

#: ../Pyblio/GnomeUI/Document.py:495
msgid ""
"The database has been externally modified.\n"
"Overwrite changes ?"
msgstr ""
"データベースが第三者によって変更されました。\n"
"変更を上書きしますか?"

#. no result.
#: ../Pyblio/GnomeUI/Document.py:524
msgid "Your query returned no result"
msgstr ""

#. error
#: ../Pyblio/GnomeUI/Document.py:528
msgid "An error occured during Medline Query"
msgstr ""

#. get a new file name
#: ../Pyblio/GnomeUI/Document.py:538
msgid "Merge file"
msgstr "ファイルの結合"

#: ../Pyblio/GnomeUI/Document.py:551 ../Pyblio/GnomeUI/Document.py:638
msgid "Open error"
msgstr "開くときのエラー"

#: ../Pyblio/GnomeUI/Document.py:575
msgid "Merge status"
msgstr "結合状態"

#. get a new file name
#: ../Pyblio/GnomeUI/Document.py:586
msgid "Open file"
msgstr "ファイルを開く"

#: ../Pyblio/GnomeUI/Document.py:624
msgid ""
"An autosave file was found which is newer than the original file.\n"
"Do you want to restore it?"
msgstr ""

#: ../Pyblio/GnomeUI/Document.py:659 ../Pyblio/GnomeUI/Document.py:800
#: ../Pyblio/GnomeUI/Document.py:860
#, python-format
msgid ""
"Unable to remove autosave file `%s':\n"
"%s"
msgstr ""
"自動保存ファイル `%s' を削除できません:\n"
"%s"

#: ../Pyblio/GnomeUI/Document.py:710
#, python-format
msgid ""
"Error during autosaving:\n"
"%s"
msgstr ""
"自動保存中にエラー:\n"
"%s"

#: ../Pyblio/GnomeUI/Document.py:738
#, python-format
msgid ""
"Unable to save `%s':\n"
"%s"
msgstr ""
"`%s'を開けません:\n"
"%s"

#: ../Pyblio/GnomeUI/Document.py:746
msgid ""
"An internal error occured during saving\n"
"Try to Save As..."
msgstr ""
"保存するときに内部エラーが発生しました\n"
"「名前を付けて保存」を試してください..."

#: ../Pyblio/GnomeUI/Document.py:761
msgid "Save As..."
msgstr "名前を付けて保存..."

#: ../Pyblio/GnomeUI/Document.py:767
#, python-format
msgid ""
"The file `%s' already exists.\n"
"Overwrite it ?"
msgstr ""
"`%s'はすでに存在します。\n"
"上書きしますか?"

#: ../Pyblio/GnomeUI/Document.py:774
#, python-format
msgid ""
"During opening:\n"
"%s"
msgstr ""
"開いているときに:\n"
"%s"

#: ../Pyblio/GnomeUI/Document.py:812
msgid "Reopen error"
msgstr "再オープンエラー"

#: ../Pyblio/GnomeUI/Document.py:885
#, python-format
msgid ""
"An entry called `%s' already exists.\n"
"Rename and add it anyway ?"
msgstr ""
"`%s'というエントリがすでに存在します。\n"
"名前を変更してとにかく追加しますか?"

#: ../Pyblio/GnomeUI/Document.py:922
msgid "Really remove all the entries ?"
msgstr "本当にすべてのエントリを削除しますか?"

#: ../Pyblio/GnomeUI/Document.py:942
msgid "Create new entry"
msgstr "新規エントリの作成"

#: ../Pyblio/GnomeUI/Document.py:956
#, python-format
msgid "Really edit %d entries ?"
msgstr "本当に%dエントリを編集しますか?"

#: ../Pyblio/GnomeUI/Document.py:1002
#, python-format
msgid "Remove all the %d entries ?"
msgstr "%dエントリをすべて削除しますか?"

#: ../Pyblio/GnomeUI/Document.py:1004
#, python-format
msgid "Remove entry `%s' ?"
msgstr "エントリ`%s'を削除しますか?"

#: ../Pyblio/GnomeUI/Document.py:1023
msgid ""
"your search text must contain\n"
"latin-1 characters only"
msgstr ""

#: ../Pyblio/GnomeUI/Document.py:1102 ../Pyblio/GnomeUI/Document.py:1110
#, python-format
msgid ""
"Can't connect to LyX:\n"
"%s"
msgstr ""
"LyXに接続できません:\n"
"%s"

#: ../Pyblio/GnomeUI/Document.py:1226
#, python-format
msgid ""
"Can't display documentation:\n"
"%s"
msgstr ""

#: ../Pyblio/GnomeUI/Document.py:1234
msgid "This program is copyrighted under the GNU GPL"
msgstr "このプログラムはGNU GPLのもとに著作権があります"

#: ../Pyblio/GnomeUI/Document.py:1235
msgid "GNOME interface to the Pybliographer system."
msgstr "PybliographerシステムのGNOMEインターフェースです。"

#: ../Pyblio/GnomeUI/Document.py:1246
msgid "GNOME Translation Team"
msgstr "GNOME翻訳チーム"

#: ../Pyblio/GnomeUI/Document.py:1251
msgid "Pybliographer Home Page"
msgstr "Pybliographerホームページ"

#: ../Pyblio/GnomeUI/Editor.py:35
msgid "Last Name, First Name"
msgstr "名前、名字"

#: ../Pyblio/GnomeUI/Editor.py:36 ../Pyblio/GnomeUI/Editor.py:1292
#: ../Pyblio/GnomeUI/Fields.py:39 ../Pyblio/GnomeUI/glade/config1.glade.h:14
#: ../Pyblio/GnomeUI/glade/fields1.glade.h:19
msgid "Text"
msgstr "テキスト"

#: ../Pyblio/GnomeUI/Editor.py:133 ../Pyblio/GnomeUI/Editor.py:1003
msgid "Error in native string parsing"
msgstr "ネイティブ文字のパースでエラー"

#: ../Pyblio/GnomeUI/Editor.py:312
msgid "Day"
msgstr "日"

#: ../Pyblio/GnomeUI/Editor.py:321
msgid "Month"
msgstr "月"

#: ../Pyblio/GnomeUI/Editor.py:330
msgid "Year"
msgstr "年"

#: ../Pyblio/GnomeUI/Editor.py:355
msgid "Invalid day field in date"
msgstr "日付の日フィールドが不適切です"

#: ../Pyblio/GnomeUI/Editor.py:363
msgid "Invalid month field in date"
msgstr "日付の月フィールドが不適切です"

#: ../Pyblio/GnomeUI/Editor.py:371
msgid "Invalid year field in date"
msgstr "日付の年フィールドが不適切です"

#. A delete button
#: ../Pyblio/GnomeUI/Editor.py:419 ../Pyblio/GnomeUI/Search.py:86
msgid "Delete"
msgstr "削除"

#: ../Pyblio/GnomeUI/Editor.py:430 ../Pyblio/GnomeUI/Editor.py:443
#: ../Pyblio/GnomeUI/Editor.py:446
msgid "[ Drop an Entry here ]"
msgstr ""

#: ../Pyblio/GnomeUI/Editor.py:502
msgid "Browse..."
msgstr "参照..."

#: ../Pyblio/GnomeUI/Editor.py:525
msgid "Select file"
msgstr "ファイルの選択"

#: ../Pyblio/GnomeUI/Editor.py:549 ../Pyblio/GnomeUI/Fields.py:236
#: ../Pyblio/GnomeUI/glade/config1.glade.h:8
#: ../Pyblio/GnomeUI/glade/fields1.glade.h:6
msgid "Entry type"
msgstr "エントリタイプ"

#: ../Pyblio/GnomeUI/Editor.py:551
msgid "Key"
msgstr "キー"

#: ../Pyblio/GnomeUI/Editor.py:600
msgid "Create Field"
msgstr "フィールドの作成"

#. navigation buttons
#: ../Pyblio/GnomeUI/Editor.py:605
msgid "Back"
msgstr "戻る"

#: ../Pyblio/GnomeUI/Editor.py:608
msgid "Next"
msgstr "進む"

#: ../Pyblio/GnomeUI/Editor.py:728 ../Pyblio/GnomeUI/Fields.py:451
msgid "Mandatory"
msgstr "必須"

#: ../Pyblio/GnomeUI/Editor.py:728 ../Pyblio/GnomeUI/Fields.py:452
msgid "Optional"
msgstr "オプション"

#: ../Pyblio/GnomeUI/Editor.py:728 ../Pyblio/GnomeUI/Editor.py:1207
msgid "Notes"
msgstr "メモ"

#: ../Pyblio/GnomeUI/Editor.py:728
msgid "Extra"
msgstr "おまけ"

#: ../Pyblio/GnomeUI/Editor.py:909
msgid "Invalid key format"
msgstr "不適切なキーのフォーマット"

#: ../Pyblio/GnomeUI/Editor.py:917
#, python-format
msgid "Key `%s' already exists"
msgstr "キー`%s'はすでに存在します"

#: ../Pyblio/GnomeUI/Editor.py:934
#, python-format
msgid "The `%s' field contains a non Latin-1 symbol"
msgstr ""

#: ../Pyblio/GnomeUI/Editor.py:996
msgid "Your text contains non Latin-1 symbols"
msgstr ""

#: ../Pyblio/GnomeUI/Editor.py:1023
msgid "Edit entry"
msgstr "エントリの編集"

#: ../Pyblio/GnomeUI/Editor.py:1037 ../Pyblio/GnomeUI/Editor.py:1102
msgid "Native Editing"
msgstr "ネイティブ編集"

#: ../Pyblio/GnomeUI/Editor.py:1125
msgid "Standard Editing"
msgstr "標準編集"

#: ../Pyblio/GnomeUI/Editor.py:1229
msgid "mandatory"
msgstr "必須"

#: ../Pyblio/GnomeUI/Editor.py:1295
msgid "Enter text here"
msgstr "文字列をここに入力してください"

#: ../Pyblio/GnomeUI/Editor.py:1347
msgid "New Annotation Name"
msgstr ""

#: ../Pyblio/GnomeUI/Editor.py:1353
msgid "Name of the new annotation:"
msgstr ""

#: ../Pyblio/GnomeUI/Entry.py:114
#, python-format
msgid ""
"Cannot open URL:\n"
"%s"
msgstr ""
"URL を開くことができません:\n"
"%s"

#: ../Pyblio/GnomeUI/Fields.py:38
msgid "Authors"
msgstr "著者"

#: ../Pyblio/GnomeUI/Fields.py:40
msgid "Long Text"
msgstr "長いテキスト"

#: ../Pyblio/GnomeUI/Fields.py:41 ../Pyblio/GnomeUI/glade/config1.glade.h:16
#: ../Pyblio/GnomeUI/glade/fields1.glade.h:21
msgid "URL"
msgstr "URL"

#: ../Pyblio/GnomeUI/Fields.py:42
msgid "Reference"
msgstr "参照"

#: ../Pyblio/GnomeUI/Fields.py:43
msgid "Date"
msgstr "日付"

#: ../Pyblio/GnomeUI/Fields.py:124 ../Pyblio/GnomeUI/glade/config1.glade.h:11
#: ../Pyblio/GnomeUI/glade/fields1.glade.h:10
msgid "Name"
msgstr "名前"

#: ../Pyblio/GnomeUI/Fields.py:127 ../Pyblio/GnomeUI/glade/config1.glade.h:15
#: ../Pyblio/GnomeUI/glade/fields1.glade.h:20
msgid "Type"
msgstr "タイプ"

#: ../Pyblio/GnomeUI/Fields.py:300
msgid "Available"
msgstr ""

#: ../Pyblio/GnomeUI/Fields.py:310
msgid "Associated"
msgstr ""

#: ../Pyblio/GnomeUI/Fields.py:321
msgid "Please, select an entry type from previous page."
msgstr ""

#: ../Pyblio/GnomeUI/Fields.py:332
#, python-format
msgid "Fields associated with <b>%s</b> entry type"
msgstr ""

#: ../Pyblio/GnomeUI/FileSelector.py:39
msgid "File"
msgstr "ファイル"

#: ../Pyblio/GnomeUI/FileSelector.py:76
#: ../Pyblio/GnomeUI/glade/openurl.glade.h:2
msgid "Bibliography type:"
msgstr "文献タイプ"

#: ../Pyblio/GnomeUI/FileSelector.py:93 ../Pyblio/GnomeUI/OpenURL.py:61
msgid " - According to file suffix - "
msgstr ""

#. The root of the search tree is the full database
#: ../Pyblio/GnomeUI/Search.py:71
msgid "Full database"
msgstr "全データベース"

#: ../Pyblio/GnomeUI/Search.py:142
msgid "internal error during evaluation"
msgstr "評価の途中の内部エラー"

#: ../Pyblio/GnomeUI/Search.py:196
#, python-format
msgid ""
"while compiling %s\n"
"error: %s"
msgstr ""
"%sをコンパイルしているときに\n"
"エラー: %s"

#. These are additional search fields
#: ../Pyblio/GnomeUI/Sort.py:97
msgid "[Entry Type]"
msgstr "[エントリ タイプ]"

#: ../Pyblio/GnomeUI/Sort.py:98
msgid "[Key Value]"
msgstr "[キー 値]"

#: ../Pyblio/GnomeUI/Utils.py:193
msgid "The following errors occured:\n"
msgstr "次のエラーが発生しました:\n"

#: ../Pyblio/GnomeUI/__init__.py:45
#, python-format
msgid "This is Pybliographic %s [Python %s, Gtk %s, PyGTK %s]"
msgstr ""

#: ../Pyblio/GnomeUI/glade/config1.glade.h:1
#: ../Pyblio/GnomeUI/glade/fields1.glade.h:1
#: ../Pyblio/GnomeUI/glade/medline.glade.h:1
#: ../Pyblio/GnomeUI/glade/openurl.glade.h:1
#: ../Pyblio/GnomeUI/glade/search.glade.h:1
msgid "*"
msgstr ""

#: ../Pyblio/GnomeUI/glade/config1.glade.h:2
msgid "<b>Entry type</b>"
msgstr "<b>エントリタイプ</b>"

#: ../Pyblio/GnomeUI/glade/config1.glade.h:3
msgid "A"
msgstr ""

#: ../Pyblio/GnomeUI/glade/config1.glade.h:4
#: ../Pyblio/GnomeUI/glade/fields1.glade.h:2
msgid "Association"
msgstr ""

#: ../Pyblio/GnomeUI/glade/config1.glade.h:5
#: ../Pyblio/GnomeUI/glade/fields1.glade.h:3
msgid "Datum"
msgstr ""

#: ../Pyblio/GnomeUI/glade/config1.glade.h:6
#: ../Pyblio/GnomeUI/glade/fields1.glade.h:4
msgid "Editor"
msgstr "編集"

#: ../Pyblio/GnomeUI/glade/config1.glade.h:7
#: ../Pyblio/GnomeUI/glade/fields1.glade.h:5
msgid "Entries"
msgstr "エントリ"

#: ../Pyblio/GnomeUI/glade/config1.glade.h:9
msgid "FIELDS DEFINITION"
msgstr "フィールド定義"

#: ../Pyblio/GnomeUI/glade/config1.glade.h:10
#: ../Pyblio/GnomeUI/glade/fields1.glade.h:8
msgid "Fields "
msgstr "フィールド"

#: ../Pyblio/GnomeUI/glade/config1.glade.h:12
#: ../Pyblio/GnomeUI/glade/fields1.glade.h:11
msgid "Name "
msgstr "名前"

#: ../Pyblio/GnomeUI/glade/config1.glade.h:13
msgid "Pybliographer Configuration"
msgstr "Pybliographer の設定"

#: ../Pyblio/GnomeUI/glade/config1.glade.h:17
msgid "standard fields"
msgstr "標準フィールド"

#: ../Pyblio/GnomeUI/glade/fields1.glade.h:7
msgid "Entry types and field names configuration"
msgstr ""

#: ../Pyblio/GnomeUI/glade/fields1.glade.h:9
msgid "Information for <b>Article</b> entry type"
msgstr ""

#: ../Pyblio/GnomeUI/glade/fields1.glade.h:12
msgid ""
"Select an entry type and specify \n"
"its associated (mandatory and\n"
"optional) fields on the next page.\n"
"\n"
"\n"
"\n"
msgstr ""

#: ../Pyblio/GnomeUI/glade/format.glade.h:1
msgid "Bibliography style:"
msgstr "目録スタイル:"

#: ../Pyblio/GnomeUI/glade/format.glade.h:2
msgid "Format entries"
msgstr "エントリのフォーマット"

#: ../Pyblio/GnomeUI/glade/format.glade.h:3
msgid "Output file:"
msgstr "出力ファイル:"

#: ../Pyblio/GnomeUI/glade/format.glade.h:4
msgid "Output format:"
msgstr "出力フォーマット:"

#: ../Pyblio/GnomeUI/glade/format.glade.h:5
msgid "Save Formatted as"
msgstr ""

#: ../Pyblio/GnomeUI/glade/format.glade.h:6
msgid "Style Selection"
msgstr "スタイルの選択"

#: ../Pyblio/GnomeUI/glade/medline.glade.h:2
msgid "Enter your Medline Query"
msgstr ""

#: ../Pyblio/GnomeUI/glade/medline.glade.h:3
msgid "From:"
msgstr ""

#: ../Pyblio/GnomeUI/glade/medline.glade.h:4
msgid "Limited to:"
msgstr ""

#: ../Pyblio/GnomeUI/glade/medline.glade.h:5
msgid ""
"Maximum number\n"
"of results:"
msgstr ""

#: ../Pyblio/GnomeUI/glade/medline.glade.h:7
msgid "Medline Query"
msgstr ""

#: ../Pyblio/GnomeUI/glade/medline.glade.h:8
msgid ""
"Only items \n"
"ahead of print"
msgstr ""

#: ../Pyblio/GnomeUI/glade/medline.glade.h:10
msgid ""
"Only items \n"
"with abstracts"
msgstr ""

#: ../Pyblio/GnomeUI/glade/medline.glade.h:12
msgid ""
"Search \n"
"PubMed for:"
msgstr ""

#: ../Pyblio/GnomeUI/glade/medline.glade.h:14
msgid ""
"Starting listing at\n"
"result number:"
msgstr ""

#: ../Pyblio/GnomeUI/glade/medline.glade.h:16
msgid "To:"
msgstr ""

#: ../Pyblio/GnomeUI/glade/medline.glade.h:17
msgid ""
"Use the format YYYY/MM/DD\n"
"Month and day are optional."
msgstr ""

#: ../Pyblio/GnomeUI/glade/openurl.glade.h:3
msgid "Enter the location (URI) of the file you would like to open:"
msgstr ""

#: ../Pyblio/GnomeUI/glade/openurl.glade.h:4
msgid "Open Location"
msgstr "場所を開く"

#: ../Pyblio/GnomeUI/glade/pyblio.glade.in.h:1
msgid "Cite"
msgstr "引用"

#: ../Pyblio/GnomeUI/glade/pyblio.glade.in.h:3
msgid "Contents"
msgstr ""

#: ../Pyblio/GnomeUI/glade/pyblio.glade.in.h:9
msgid "Open File"
msgstr "ファイルを開く"

#: ../Pyblio/GnomeUI/glade/pyblio.glade.in.h:10
msgid "Previous Documents"
msgstr "前のドキュメント"

#: ../Pyblio/GnomeUI/glade/pyblio.glade.in.h:11
msgid "Pybliographer"
msgstr "Pybliographer"

#: ../Pyblio/GnomeUI/glade/pyblio.glade.in.h:13
msgid "Save File"
msgstr ""

#: ../Pyblio/GnomeUI/glade/pyblio.glade.in.h:14
msgid "Settings"
msgstr "設定"

#: ../Pyblio/GnomeUI/glade/pyblio.glade.in.h:15
msgid "_Add..."
msgstr "追加(_A)..."

#: ../Pyblio/GnomeUI/glade/pyblio.glade.in.h:16
msgid "_Delete..."
msgstr "削除(_D)..."

#: ../Pyblio/GnomeUI/glade/pyblio.glade.in.h:17
msgid "_Edit..."
msgstr "編集(_E)..."

#: ../Pyblio/GnomeUI/glade/pyblio.glade.in.h:18
msgid "_New"
msgstr "新規(_N)"

#: ../Pyblio/GnomeUI/glade/search.glade.h:2
msgid "Close"
msgstr "閉じる"

#: ../Pyblio/GnomeUI/glade/search.glade.h:3
msgid "Expert Search"
msgstr "上級検索"

#: ../Pyblio/GnomeUI/glade/search.glade.h:4
msgid "Field"
msgstr "フィールド"

#: ../Pyblio/GnomeUI/glade/search.glade.h:5
msgid "Pattern"
msgstr "パターン"

#: ../Pyblio/GnomeUI/glade/search.glade.h:6
msgid "Query"
msgstr ""

#: ../Pyblio/GnomeUI/glade/search.glade.h:7
msgid "Search"
msgstr "検索"

#: ../Pyblio/GnomeUI/glade/search.glade.h:8
msgid "Simple Search"
msgstr "簡単検索"

#: ../Pyblio/GnomeUI/glade/sort.glade.h:1
msgid "Select sort criterions"
msgstr "ソート基準の選択"

#: ../Pyblio/GnomeUI/glade/sort.glade.h:2
msgid "Set as default"
msgstr ""

#: ../Pyblio/LyX.py:37
#, python-format
msgid "no input pipe `%s'"
msgstr "パイプ`%s'に入力がありません"

#: ../Pyblio/LyX.py:43
#, python-format
msgid "no output pipe `%s'"
msgstr "パイプ`%s'に出力がありません"

#: ../Pyblio/Open.py:96
#, python-format
msgid "method `%s' provides no opener"
msgstr "メソッド`%s'にはオープナーがありません"

#: ../Pyblio/Open.py:104 ../Pyblio/Open.py:151
#, python-format
msgid "File `%s' does not exist"
msgstr "ファイル`%s'は存在しません"

#: ../Pyblio/Open.py:115 ../Pyblio/Open.py:162
#, python-format
msgid "don't know how to open `%s'"
msgstr "`%s'をどうやって開くのか分かりません"

#: ../Pyblio/Open.py:143
#, python-format
msgid "method `%s' provides no iterator"
msgstr "メソッド`%s'にはイテレータがありません"

#: ../Pyblio/Resource.py:77
#, python-format
msgid ""
"Warning: This URL is marked as Invalid or Approximate: %s\n"
"Continue?"
msgstr ""

#: ../Pyblio/Resource.py:81
msgid "Determining Mime Type ... "
msgstr ""

#: ../Pyblio/Resource.py:86
#, python-format
msgid "Cannot determine mime type for item %s "
msgstr ""

#: ../Pyblio/Resource.py:87
#, python-format
msgid ""
"URL in question is: %s\n"
"You should check the url or path given for errors.\n"
"Details: %s"
msgstr ""

#: ../Pyblio/Resource.py:98
msgid "Accessing resource ..."
msgstr ""

#: ../Pyblio/Resource.py:116
#, python-format
msgid "IOError for item %s: cannot uncompress resource."
msgstr ""

#: ../Pyblio/Resource.py:117
#, python-format
msgid ""
"URL: %s\n"
"Details: %s"
msgstr ""

#: ../Pyblio/Resource.py:137
msgid "Starting application ..."
msgstr ""

#: ../Pyblio/Resource.py:140
msgid "No application to view resource"
msgstr ""

#: ../Pyblio/Resource.py:141
#, python-format
msgid ""
"For mime type %s, no entry found in \n"
"configuration option resource/viewers.\n"
"Please consider adding one.\n"
"URL: %s"
msgstr ""

#: ../Pyblio/Style/Parser.py:261
#, python-format
msgid "missing '%s' attribute"
msgstr ""

#: ../Pyblio/Style/Parser.py:271
#, python-format
msgid "invalid opening tag: %s"
msgstr "正しくない開きタグ: %s"

#: ../Pyblio/Style/Parser.py:281
#, python-format
msgid "invalid closing tag: %s"
msgstr "正しくない閉じタグ: %s"

#: ../pybliographer.py:43
#, python-format
msgid "This is %s, version %s"
msgstr "これは%sで、バージョンは%sです"

#: ../pybliographer.py:47 ../pybliographer.py:54
msgid "This is free software with ABSOLUTELY NO WARRANTY."
msgstr "これはまったく保証のないフリーソフトウェアです。"

#: ../pybliographer.py:48
msgid "For details, type `warranty'."
msgstr "詳しくは、`warranty'と入力して下さい。"

#: ../pybliographer.py:111 ../pybliographer.py:163
#, python-format
msgid "%s: error: can't open file `%s'"
msgstr "%s: エラー: ファイル`%s'を開けません"

#: ../pybliographer.py:124
#, python-format
msgid "For help, run %s and type `help' at the prompt"
msgstr "ヘルプを見るには、%sを実行してプロンプトで`help'と入力してください"

#: ../pybliographer.py:133
msgid ""
"Useful commands:\n"
"\thelp     to get some help\n"
"\tquit     to quit\n"
msgstr ""
"使用可能なコマンド:\n"
"\thelp     でヘルプをいくらか見れます\n"
"\tquit     で終了します\n"

#. save history
#: ../pybliographic.desktop.in.h:1
msgid "Bibliography Manager"
msgstr ""

#: ../pybliographic.desktop.in.h:2
msgid "Manage bibliographic databases"
msgstr ""

#: ../pybliographic.desktop.in.h:3
msgid "Pybliographic Bibliography Manager"
msgstr ""

#: ../scripts/pybliocheck.py:31
msgid "usage: pybliocheck <file | directory>..."
msgstr ""

#: ../scripts/pybliocheck.py:60
#, python-format
msgid "file `%s' is ok [%d entries]"
msgstr ""

#: ../scripts/pybliocompact.py:30
msgid "usage: pybliocompact <latexfile> <bibtexfiles...>"
msgstr "使い方: pybliocompact <latexファイル> <bibtexファイル...>"

#: ../scripts/pybliocompact.py:34
#, python-format
msgid "pybliocompact: error: %s\n"
msgstr "pybliocompact: エラー: %s\n"

#: ../scripts/pybliocompact.py:99
msgid "no entry"
msgstr "エントリなし"

#: ../scripts/pybliocompact.py:162 ../scripts/pybliotext.py:240
#, python-format
msgid "can't find the following entries: %s"
msgstr "次のエントリを検索できません: %s"

#: ../scripts/pyblioconvert.py:29
msgid "usage: pyblioconvert <source>..<target> <input> [output]"
msgstr "使い方: pyblioconvert <ソース>..<ターゲット> <入力> [出力]"

#: ../scripts/pyblioconvert.py:38
msgid "pyblioconvert: error: bad conversion format"
msgstr "pyblioconvert: エラー: 変換フォーマットが間違ってます"

#: ../scripts/pyblioformat.py:34
msgid ""
"usage: pyblioformat [options] <database...>\n"
"\n"
"    options:\n"
"      -o file, --output=file\t\tspecify an output filename\n"
"      -s style, --style=style\t\tspecify a bibliography style\n"
"      -f format, --format=format\tspecify an output format\n"
"      -H header, --header=header\tdefines a header file\n"
"      -F footer, --footer=footer\tdefines a footer file\n"
"      -l 'output', --list='output'\tlists the available output formats\n"
"      -h, --help\t\t\tshow this help message\n"
"    "
msgstr ""

#: ../scripts/pyblioformat.py:48
#, python-format
msgid "pyblioformat: error: %s\n"
msgstr "pyblioformat: エラー: %s\n"

#: ../scripts/pyblioformat.py:54
#, python-format
msgid "pyblioformat: warning: %s\n"
msgstr "pyblioformat: 注意: %s\n"

#: ../scripts/pyblioformat.py:93 ../scripts/pybliotext.py:252
#: ../scripts/pybliotext.py:278 ../scripts/pybliotext.py:321
#, python-format
msgid "can't open `%s': %s"
msgstr "ファイル`%s'を開けません: %s"

#: ../scripts/pyblioformat.py:100
#, python-format
msgid "unknown list `%s'"
msgstr ""

#: ../scripts/pyblioformat.py:103
#, python-format
msgid "pyblioformat: available values for `%s':"
msgstr ""

#: ../scripts/pyblioformat.py:107
#, python-format
msgid "empty value list `%s'"
msgstr ""

#: ../scripts/pyblioformat.py:129
msgid "too few arguments"
msgstr ""

#: ../scripts/pyblioformat.py:136 ../scripts/pybliotext.py:117
#, python-format
msgid "unknown output format `%s'"
msgstr ""

#: ../scripts/pyblioformat.py:149 ../scripts/pybliotex.py:86
#: ../scripts/pybliotext.py:94
#, python-format
msgid "can't find style `%s'"
msgstr "スタイル`%s'を検索できません"

#: ../scripts/pyblioformat.py:152
#, python-format
msgid "pyblioformat: using style `%s', format `%s'\n"
msgstr ""

#: ../scripts/pyblioformat.py:167
#, python-format
msgid "can't open header `%s': %s"
msgstr "ヘッダー '%s' を開けません: %s"

#: ../scripts/pyblioformat.py:175
#, python-format
msgid "can't open database: %s"
msgstr "データベースを開けません: %s"

#: ../scripts/pyblioformat.py:190
#, python-format
msgid "can't open footer `%s': %s"
msgstr "フッター '%s' を開けません: %s"

#: ../scripts/pybliotex.py:34
msgid "usage: pybliotex <latexfile> [bibtexfiles...]"
msgstr "使い方: pybliotex <latexファイル> [bibtexファイル...]"

#: ../scripts/pybliotex.py:38
#, python-format
msgid "pybliotex: error: %s\n"
msgstr "pybliotex: エラー: %s\n"

#. warn the user that some entries were not found
#: ../scripts/pybliotex.py:59
msgid "pybliotex: warning: the following keys were not resolved"
msgstr "pybliotex: 注意: 次のキーは解決されていません"

#. If the LaTeX document declares no style...
#: ../scripts/pybliotex.py:64
msgid "no style defined"
msgstr "スタイルが定義されていません"

#: ../scripts/pybliotext.py:41
msgid "usage: pybliotext [-o outputfile] [-s style] <textfile> <bibfiles...>"
msgstr ""
"使い方: pybliotext [-o 出力ファイル] [-s スタイル] <textファイル> <bibファイ"
"ル...>"

#: ../scripts/pybliotext.py:45
#, python-format
msgid "pybliotext: error: %s\n"
msgstr "pybliotext: エラー: %s\n"

#: ../scripts/pybliotext.py:50
#, python-format
msgid "pybliotext: warning: %s\n"
msgstr "pybliotext: 警告: %s\n"

#: ../scripts/pybliotext.py:124
#, python-format
msgid "File already exists: `%s'"
msgstr "ファイルが既に存在しています: `%s'"

#: ../scripts/pybliotext.py:127
#, python-format
msgid "A file with the same name already exists: `%s'"
msgstr ""

#: ../scripts/pybliotext.py:178
msgid "no citation found"
msgstr ""

#: ../scripts/pybliotext.py:193
#, python-format
msgid "pybliotext: using style `%s', format `%s'\n"
msgstr ""

#: ../scripts/pybliotext.py:316
#, python-format
msgid "can't create `%s'"
msgstr "`%s' を作成できません"

#: ../scripts/pybliotext.py:342
#, python-format
msgid "can't remove `%s'"
msgstr "`%s'を削除できません"

#: ../scripts/pybliotext.py:344
msgid "Done"
msgstr "完了"

#~ msgid "Choose your preferences"
#~ msgstr "設定の選択"

#~ msgid ""
#~ "Please install bug-buddy\n"
#~ "to use this feature"
#~ msgstr ""
#~ "この機能を使うにはbug-buddyを\n"
#~ "インストールしてください"

#~ msgid "Entries configuration"
#~ msgstr "エントリ設定"

#~ msgid "Entry"
#~ msgstr "エントリ"

#~ msgid "Entry Name:"
#~ msgstr "エントリ名:"

#~ msgid "Status"
#~ msgstr "状態"

#~ msgid "Add..."
#~ msgstr "追加..."

#~ msgid "Edit..."
#~ msgstr "編集..."

#, fuzzy
#~ msgid "Delete..."
#~ msgstr "削除"

#~ msgid "usage: pyblioformat [bibtexfiles...]\n"
#~ msgstr "使い方: pyblioformat [bibtexファイル...]\n"

#, fuzzy
#~ msgid "Entries..."
#~ msgstr "エントリ(_E)..."

#, fuzzy
#~ msgid "Search Command:"
#~ msgstr "検索コマンド:"

#~ msgid "Sort criterions"
#~ msgstr "ソート基準"

#~ msgid "Groups the active fields on the top of the list"
#~ msgstr "リストの先頭にあるアクティブなフィールドをグループ化"

#~ msgid "Unselect all"
#~ msgstr "すべて選択解除"

#~ msgid "Removes all the sort criterions"
#~ msgstr "すべてのソート基準を削除"

#~ msgid "_Previous Documents"
#~ msgstr "前のドキュメント(_P)"

#~ msgid "Submit a Bug Report"
#~ msgstr "バグレポートの提出"

#~ msgid "_Delete"
#~ msgstr "削除(_D)"

#~ msgid "Open"
#~ msgstr "開く"

#~ msgid "Save"
#~ msgstr "保存"

#~ msgid "Find"
#~ msgstr "検索"

#~ msgid "Select output file"
#~ msgstr "出力ファイルの選択"

#~ msgid ""
#~ "Item `%s':\n"
#~ "\n"
#~ "%s"
#~ msgstr ""
#~ "アイテム`%s':\n"
#~ "\n"
#~ "%s"

#~ msgid "True"
#~ msgstr "真"

#~ msgid "False"
#~ msgstr "偽"

#~ msgid "Value"
#~ msgstr "値"

#~ msgid "Fields configuration"
#~ msgstr "フィールド設定"

#~ msgid "Name:"
#~ msgstr "名前:"

#~ msgid "Type:"
#~ msgstr "タイプ:"

#~ msgid "BibTeX"
#~ msgstr "BibTeX"

#~ msgid "BibTeX parser configuration"
#~ msgstr "BibTeXパーサ設定"

#~ msgid "Strict Parsing"
#~ msgstr "厳密なパース"

#~ msgid "Turn warnings into errors"
#~ msgstr "警告もエラーにする"

#~ msgid "Macros"
#~ msgstr "マクロ"

#~ msgid "BibTeX @String{} macros"
#~ msgstr "BibTeX @String{} マクロ"

#~ msgid "Date Fields"
#~ msgstr "データフィールド"

#~ msgid "Links from `real' date field to the two bibtex"
#~ msgstr "`実際の'日付フィールドから2つのbibtexへのリンク"

#~ msgid "Month values"
#~ msgstr "月の値"

#~ msgid " A hash table linking month names to their"
#~ msgstr " ハッシュテーブルからのリンクが月名から"

#~ msgid "Base"
#~ msgstr "ベース"

#~ msgid "Elementary configuration"
#~ msgstr "要素の設定"

#~ msgid "Advertise"
#~ msgstr "広告"

#~ msgid "Specify wether or not the program should add a message"
#~ msgstr "プログラムがメッセージを追加するかどうかを指定してください"

#~ msgid "Existing entries"
#~ msgstr "存在するエントリ"

#~ msgid "Default Type"
#~ msgstr "デフォルトタイプ"

#~ msgid "Default type for a newly created entry"
#~ msgstr "新規に作成されたエントリのデフォルトタイプ"

#~ msgid "LyX Path"
#~ msgstr "LyX パス"

#~ msgid "Path to the LyX server"
#~ msgstr "LyXサーバへのパス"

#~ msgid "Path"
#~ msgstr "パス"

#~ msgid "Cross Reference"
#~ msgstr "クロスリファレンス"

#~ msgid "Description"
#~ msgstr "説明"

#~ msgid "Address 1"
#~ msgstr "アドレス 1"

#~ msgid "Address 2"
#~ msgstr "アドレス 2"

#~ msgid "Title"
#~ msgstr "タイトル"

#~ msgid "Specific Title"
#~ msgstr "特定できるタイトル"

#~ msgid "Journal"
#~ msgstr "ジャーナル"

#~ msgid "Special"
#~ msgstr "特別"

#~ msgid "Book Title"
#~ msgstr "本のタイトル"

#~ msgid "Subject"
#~ msgstr "題名"

#~ msgid "Ownership"
#~ msgstr "所有者"

#~ msgid "Series"
#~ msgstr "シリーズ"

#~ msgid "Edition"
#~ msgstr "版"

#~ msgid "Volume"
#~ msgstr "巻"

#~ msgid "Number"
#~ msgstr "番号"

#~ msgid "Chapter"
#~ msgstr "章"

#~ msgid "Pages"
#~ msgstr "ページ"

#~ msgid "School"
#~ msgstr "学校"

#~ msgid "Organization"
#~ msgstr "所属"

#~ msgid "Dates"
#~ msgstr "日付"

#~ msgid "Institution"
#~ msgstr "学会"

#~ msgid "Publisher"
#~ msgstr "発行人"

#~ msgid "Address"
#~ msgstr "アドレス"

#~ msgid "Format"
#~ msgstr "フォーマット"

#~ msgid "# Series"
#~ msgstr "# シリーズ"

#~ msgid "Conference Place"
#~ msgstr "会議の場所"

#~ msgid "IEEECN"
#~ msgstr "IEEECN"

#~ msgid "LoCN"
#~ msgstr "LoCN"

#~ msgid "ISBN"
#~ msgstr "ISBN"

#~ msgid "ISSN"
#~ msgstr "ISSN"

#~ msgid "Language"
#~ msgstr "言語"

#~ msgid "How Published"
#~ msgstr "発行の経緯"

#~ msgid "To Appear"
#~ msgstr "出版"

#~ msgid "Received"
#~ msgstr "受取"

#~ msgid "Owner"
#~ msgstr "所有者"

#~ msgid "Keywords"
#~ msgstr "キーワード"

#~ msgid "Abstract"
#~ msgstr "概要"

#~ msgid "Remarks"
#~ msgstr "リマーク"

#~ msgid "Article"
#~ msgstr "記事"

#~ msgid " "
#~ msgstr " "

#~ msgid "Book"
#~ msgstr "本"

#~ msgid "Booklet"
#~ msgstr "本棚"

#~ msgid "In Book"
#~ msgstr "本の中"

#~ msgid "In Collection"
#~ msgstr "コレクションの中"

#~ msgid "In Proceedings"
#~ msgstr "議事録の中"

#~ msgid "Manual"
#~ msgstr "マニュアル"

#~ msgid "Masters Thesis"
#~ msgstr "修士論文"

#~ msgid "Miscellanous"
#~ msgstr "その他"

#~ msgid "PhD Thesis"
#~ msgstr "PhD論文"

#~ msgid "Proceedings"
#~ msgstr "議事録"

#~ msgid "Technical Report"
#~ msgstr "テクニカルレポート"

#~ msgid "Unpublished"
#~ msgstr "未発行"

#~ msgid "Default type"
#~ msgstr "デフォルトタイプ"

#~ msgid "Default type for an Ovid entry"
#~ msgstr "Ovidエントリのデフォルトタイプ"

#~ msgid "Mapping"
#~ msgstr "マッピング"

#~ msgid "A mapping between the Ovid field name and the"
#~ msgstr "Ovidフィールド名とのマッピング"

#~ msgid "Field names correspondances"
#~ msgstr "フィールド名の対応"

#~ msgid "Gnome"
#~ msgstr "Gnome"

#~ msgid "Gnome interface configuration"
#~ msgstr "Gnomeインターフェース設定"

#~ msgid "Columns"
#~ msgstr "カラム"

#~ msgid "Fields displayed on the main screen of the interface"
#~ msgstr "インターフェースのメインスクリーンに表示されるフィールド"

#~ msgid "Tooltips"
#~ msgstr "ツールチップ"

#~ msgid "Enable tooltips ?"
#~ msgstr "ツールチップを有効にしますか?"

#~ msgid "Native Edit"
#~ msgstr "ネイティブ編集"

#~ msgid "Edit the entries in their native format by default ?"
#~ msgstr "デフォルトではエントリをネイティブなフォーマットで編集しますか?"

#~ msgid "Use native edit by default ?"
#~ msgstr "デフォルトでネイティブ編集を使用しますか?"

#~ msgid "Searchable"
#~ msgstr "検索可能"

#~ msgid "Searchable fields"
#~ msgstr "検索可能フィールド"

#~ msgid "History Size"
#~ msgstr "履歴サイズ"

#~ msgid "Size of the history file"
#~ msgstr "履歴ファイルのサイズ"

#~ msgid "Number of items"
#~ msgstr "アイテムの数"

#~ msgid "Medline Mapping"
#~ msgstr "Medlineマッピング"

#~ msgid "Medline field names correspondances"
#~ msgstr "Medlineフィールド名の対応"

#~ msgid "Internal Error"
#~ msgstr "内部エラー"
